from src.example_package_chechump.example import add_one

def test_one_plus_one_is_two():
    assert add_one(1) == 2
