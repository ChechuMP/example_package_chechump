install:
	pip install -r requirements.txt

build:
	python -m build

test:
	pytest --junitxml=report.xml

clean:
	rm -rf dist .pytest_cache **/**/__pycache__ **/*.egg-info
